$(document).ready(function() {
	$("#quarter, #semester").select2({
		placeholder: "-",
		allowClear: false,
		formatNoMatches: "No results"
	});
	$("#day, #month, #week").select2({
		placeholder: "--",
		allowClear: false,
		formatNoMatches: "No results"
	});
	$("#year, #year2").select2({
		placeholder: "----",
		allowClear: false,
		formatNoMatches: "No results"
	});
	$("#hours").select2({
		placeholder: "--:00",
		allowClear: false,
		formatNoMatches: "No results"
	});
	$(".biannual, .quarterly, .monthly, .weekly, .daily, .hourly, .minutely").hide();
	
	$('.frequency .pt-tree-node').click(function(){
		$('.pt-tree-node.pt-active').removeClass('pt-active');
		$(this).addClass('pt-active');
	});
	
	$('.fannual').click(function(){
		$(".biannual, .quarterly, .monthly, .weekly, .daily, .hourly, .minutely").hide();
		$(".annual").show();
	});
	$('.fbiannual').click(function(){
		$(".annual, .quarterly, .monthly, .weekly, .daily, .hourly, .minutely").hide();
		$(".biannual").show();
	});
	$('.fquarterly').click(function(){
		$(".annual, .biannual, .monthly, .weekly, .daily, .hourly, .minutely").hide();
		$(".quarterly").show();
	});
	$('.fmonthly').click(function(){
		$(".annual, .biannual, .quarterly, .weekly, .daily, .hourly, .minutely").hide();
		$(".monthly").show();
	});
	$('.fweekly').click(function(){
		$(".annual, .biannual, .quarterly, .monthly, .daily, .hourly, .minutely").hide();
		$(".weekly").show();
	});
	$('.fdaily').click(function(){
		$(".annual, .biannual, .quarterly, .monthly, .weekly, .hourly, .minutely").hide();
		$(".daily").show();
	});
	$('.fhourly').click(function(){
		$(".annual, .biannual, .quarterly, .monthly, .weekly, .daily, .minutely").hide();
		$(".hourly").show();
	});
	$('.fminutely').click(function(){
		$(".annual, .biannual, .quarterly, .monthly, .weekly, .daily, .hourly").hide();
		$(".minutely").show();
	});
	
});