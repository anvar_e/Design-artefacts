$(document).ready(function () {
	
	$( ".facet" ).wrapInner( "<div class='new'></div>");
	$( ".btn-mainmenu a" ).wrapInner( "<div class='pff'></div>");
	$( ".pt-tag" ).wrap( "<span class='hh'></span>");
	
	var e = $('.resultx');
    for (var i = 0; i < 35; i++) {
      e.clone().insertAfter(e);
    }
	
	$(".bookmark").click(function() {
		$(this).html($(this).html() == "bookmark_border" ? "bookmark" : "bookmark_border");
		/*$(this).text("bookmark");*/
		$(this).toggleClass("active");
		return false;
	});
	
	$(".checked").prop("checked", true);
	$(".disabled input[type='radio']").prop("disabled", true);
	
	$('.downloadbtn input:checkbox').change(function(){
		if($(this).is(":checked")) {
			$(this).closest('div').find('.radios').removeClass('disabled');
			$(this).closest('div').find('input[type="radio"]').prop("disabled", false);
		} else {
			$(this).closest('div').find('.radios').addClass('disabled');
			$(this).closest('div').find('input[type="radio"]').prop("disabled", true);
		}
	});
	
	// hide .navbar first
	$(".secondmenu").hide();
	
	$(function () {
		if ($(window).width() > 768) {
			$(window).scroll(function () {
				if ($(this).scrollTop() > 125) {
					// $('.secondmenu').slideDown();
					if ($(".noshow").is(":visible")) {
						$('.secondmenu').hide(); 
					} else {
						$('.secondmenu').slideDown(); 
					}
				} else {
					$('.secondmenu').slideUp();
				}
			});
		}
	});
	
	$(".request-btn").click(function () {
		$(".shareform").hide();
		$(".sharethx").show();
	});
	
	$(".alert-badge").hide();
	$(".request-btn2").click(function () {
		$(".shareform input, .shareform button").prop("disabled", true);
		$(".alert-badge").show();
	});
	
	$(".hidethis").hide();
	$(".collapse-btn").click(function () {
		$(this).parents(".results-row").find(".hidethis").slideToggle(500);
		$(this).parents(".results-row").find("blockquote").toggleClass("active");
	});
	
	$(".faq").hide();
	$(".collapse-title").click(function () {
		$(this).parents(".faq-row").find(".faq").slideToggle(500);
	});
	
	$('.openall').hide();
	
	$('.closeall').click(function(){
		$('#demo .panel-collapse.in').collapse('hide');
		$('#demo .panel .collapse.in').removeClass('in');
		$('#demo .panel .pt-icon-standard').removeClass('pt-icon-chevron-up').addClass('pt-icon-chevron-down');
		$('#demo .panel .collapse').css('display', '');
		$('.openall').show();
		$('.closeall').hide();
		return false;
	});
	$('.openall').click(function(){
		$('#demo .panel-collapse:not(".in")').collapse('show');
		$('#demo .panel .collapse').addClass('in');
		$('#demo .fpanel .pt-icon-standard').removeClass('pt-icon-chevron-down').addClass('pt-icon-chevron-up');
		$('#demo .panel .collapse').css('display', 'block');
		$('.closeall').show();
		$('.openall').hide();
		return false;
	});
	
	$(".accordion-toggle").click(function () {
		$header = $(this);
		$collapse = $header.next();
		$collapse.slideToggle(500);
		if($(this).children('.pt-icon-standard').hasClass('pt-icon-chevron-up')) {
			$(this).children('.accordion-toggle .pt-icon-standard').removeClass('pt-icon-chevron-up');
			$(this).children('.accordion-toggle .pt-icon-standard').addClass('pt-icon-chevron-down');
		} else {
			$(this).children('.accordion-toggle .pt-icon-standard').addClass('pt-icon-chevron-up');
			$(this).children('.accordion-toggle .pt-icon-standard').removeClass('pt-icon-chevron-down');
		}
	});
	
	$(".collapse-header").click(function () {
		$header = $(this);
		$collapse = $header.next();
		$collapse.slideToggle(500);
		if($(this).children('.pt-icon-standard').hasClass('pt-icon-chevron-up')) {
			$(this).children('.collapse-header .pt-icon-standard').removeClass('pt-icon-chevron-up');
			$(this).children('.collapse-header .pt-icon-standard').addClass('pt-icon-chevron-down');
		} else {
			$(this).children('.collapse-header .pt-icon-standard').addClass('pt-icon-chevron-up');
			$(this).children('.collapse-header .pt-icon-standard').removeClass('pt-icon-chevron-down');
		}
	});
	
	$(".button").on("click", function() {
		var modal = $(this).data("modal");
		$(modal).show();
	});

	$(".modal").on("click", function(e) {
		var className = e.target.className;
		if(className === "modal" || className === "close"){
			$(this).closest(".modal").hide();
		}
	});
	
	$('#login').click(function() {
		$('div.login').slideToggle(300);
	});
	
	$('#facets a').click(function() {
		var elementClassName = $(this).attr('id');
		$('div.'+elementClassName).slideToggle(300);
		$('.noshow').not('div.'+elementClassName).slideUp(300);
		$('a').not(this).removeClass('active');
		$(this).toggleClass('active');
		$('html, body').animate({
			scrollTop: $('.or').offset().top
		}, '');
		return false;
	});
	
	$('#buttonlist a').click(function() {
		var elementClassName = $(this).attr('id');
		$('div.'+elementClassName).slideToggle(300);
		$('.noshow').not('div.'+elementClassName).slideUp(300);
		$('a').not(this).removeClass('active2');
		
		$(this).toggleClass('active2');
		$('html, body').animate({
			scrollTop: $('.or').offset().top
		}, '');
		$(element).parents('.open').find('button[data-toggle=dropdown]').dropdown('toggle');
		return false;
	});
	$('.noshow .closebox').click(function() {
		$('.noshow').slideUp(300);
		$('#buttonlist a').not(this).removeClass('active2');
		return false;
	});
	
	$('#scopelist form').hide();
	$('#scopelist a').click(function() {
		var elementClassName = $(this).attr('id');
		$('#scopelist form').slideToggle(300);
		$('.noshow').not('#scopelist form').slideUp(300);
		//$('a').not(this).removeClass('active');
		//$(this).toggleClass('active');
		return false;
	});
	
	$('#scopelist2 form').hide();
	$('#scopelist2 a').click(function() {
		var elementClassName = $(this).attr('id');
		$('#scopelist2 form').slideToggle(300);
		$('.noshow').not('#scopelist2 form').slideUp(300);
		//$('a').not(this).removeClass('active');
		//$(this).toggleClass('active');
		return false;
	});
	
	$('#scopelist3 form').hide();
	$('#scopelist3 a').click(function() {
		var elementClassName = $(this).attr('id');
		$('#scopelist3 form').slideToggle(300);
		$('.noshow').not('#scopelist3 form').slideUp(300);
		//$('a').not(this).removeClass('active');
		//$(this).toggleClass('active');
		return false;
	});
	
	$('#scopelist4 form').hide();
	$('#scopelist4 a').click(function() {
		var elementClassName = $(this).attr('id');
		$('#scopelist4 form').slideToggle(300);
		$('.noshow').not('#scopelist4 form').slideUp(300);
		//$('a').not(this).removeClass('active');
		//$(this).toggleClass('active');
		return false;
	});
	
	$(".bigsearch").focus(function(){
		$(".bigform").removeClass("blueborder");
		$(".bigform").addClass("activeform");
		$('#facets a').removeClass('active');
		$('.noshow').slideUp(300);
	}).blur(function(){
		$(".bigform").removeClass("activeform");
		$(".bigform").addClass("blueborder");
	});
	
	$('li.submenu > a').on('click',function(event){
		event.preventDefault()
		
		$(this).toggleClass('active');
		$(this).parent().find('.megamenu').first().slideToggle(300);
		$(this).parent().siblings().find('.megamenu').hide(200);
		$(this).parent().siblings().find('a').removeClass('active');
		

		//Hide menu when clicked outside
		$(this).parent().find('.megamenu').mouseleave(function(){
			var thisUI = $(this);
			$('html').click(function(){
				thisUI.hide();
				$('html').unbind('click');
				$('li.submenu > a').removeClass('active');
			});
		});
	});
	
	
	
	$(function () {
		$(".top").click(function(){
			$("html, body").animate({scrollTop:0}, "slow"); // scrolls to the top of the page
			return false;
		});
		if ($(window).width() > 768) {
			$('html, body').animate({
				scrollTop: $('#jump').offset().top - 45
			}, '');
		}
	});
	

});

function myFunction() {
	document.getElementById("myDropdown").classList.toggle("show");
}

window.onclick = function(event) {
	if (!event.target.matches('.dropbtn')) {
		var dropdowns = document.getElementsByClassName("dropdown-content");
		var i;
		for (i = 0; i < dropdowns.length; i++) {
			var openDropdown = dropdowns[i];
			if (openDropdown.classList.contains('show')) {
				openDropdown.classList.remove('show');
			}
		}
	}
}
function myFunction2() {
  /* Get the text field */
  var copyText = document.getElementById("myInput");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  alert("Copied the text: " + copyText.value);
}
function myFunction3() {
  /* Get the text field */
  var copyText = document.getElementById("myInput3");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  alert("Copied the text: " + copyText.value);
}
