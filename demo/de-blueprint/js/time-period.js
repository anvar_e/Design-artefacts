$(document).ready(function() {
	
	$("#mode2").select2({
		placeholder: "-",
		allowClear: false,
		formatNoMatches: "No results"
	});
	
	$("#frequency, #mode").select2({
		minimumResultsForSearch: Infinity
	});


	$("#quarter, #semester").select2({
		placeholder: "-",
		allowClear: false,
		formatNoMatches: "No results"
	});
	$("#day, #month, #week").select2({
		placeholder: "--",
		allowClear: false,
		formatNoMatches: "No results"
	});
	$("#year, #year2").select2({
		placeholder: "----",
		allowClear: false,
		formatNoMatches: "No results"
	});
	$("#hours").select2({
		placeholder: "--:00",
		allowClear: false,
		formatNoMatches: "No results"
	});
	
	$(".latestdata").hide();
	
	$("#mode").change(function() {
		var val = $(this).val();
		if(val === "fdaterange") {
			$(".latestdata").hide();
			$(".daterange").show();
		}
		else if(val === "flatestdata") {
			$(".daterange").hide();
			$(".latestdata").show();
		}
	});
	
	
	$(".biannual, .quarterly, .monthly, .weekly, .daily, .hourly, .minutely").hide();
	
	$("#frequency").change(function() {
		var val = $(this).val();
		if(val === "fannual") {
			$(".biannual, .quarterly, .monthly, .weekly, .daily, .hourly, .minutely").hide();
			$(".annual").show();
		}
		else if(val === "fbiannual") {
			$(".annual, .quarterly, .monthly, .weekly, .daily, .hourly, .minutely").hide();
			$(".biannual").show();
		}
		else if(val === "fquarterly") {
			$(".annual, .biannual, .monthly, .weekly, .daily, .hourly, .minutely").hide();
			$(".quarterly").show();
		}
		else if(val === "fmonthly") {
			$(".annual, .biannual, .quarterly, .weekly, .daily, .hourly, .minutely").hide();
			$(".monthly").show();
		}
		else if(val === "fweekly") {
			$(".annual, .biannual, .quarterly, .monthly, .daily, .hourly, .minutely").hide();
			$(".weekly").show();
		}
		else if(val === "fdaily") {
			$(".annual, .biannual, .quarterly, .monthly, .weekly, .hourly, .minutely").hide();
			$(".daily").show();
		}
		else if(val === "fhourly") {
			$(".annual, .biannual, .quarterly, .monthly, .weekly, .daily, .minutely").hide();
			$(".hourly").show();
		}
		else if(val === "fminutely") {
			$(".annual, .biannual, .quarterly, .monthly, .weekly, .daily, .hourly").hide();
			$(".minutely").show();
		}
	});
	
	
	/*$('.frequency .pt-tree-node').click(function(){
		$('.pt-tree-node.pt-active').removeClass('pt-active');
		$(this).addClass('pt-active');
	});
	
	$('.fannual').click(function(){
		$(".biannual, .quarterly, .monthly, .weekly, .daily, .hourly, .minutely").hide();
		$(".annual").show();
	});
	$('.fbiannual').click(function(){
		$(".annual, .quarterly, .monthly, .weekly, .daily, .hourly, .minutely").hide();
		$(".biannual").show();
	});
	$('.fquarterly').click(function(){
		$(".annual, .biannual, .monthly, .weekly, .daily, .hourly, .minutely").hide();
		$(".quarterly").show();
	});
	$('.fmonthly').click(function(){
		$(".annual, .biannual, .quarterly, .weekly, .daily, .hourly, .minutely").hide();
		$(".monthly").show();
	});
	$('.fweekly').click(function(){
		$(".annual, .biannual, .quarterly, .monthly, .daily, .hourly, .minutely").hide();
		$(".weekly").show();
	});
	$('.fdaily').click(function(){
		$(".annual, .biannual, .quarterly, .monthly, .weekly, .hourly, .minutely").hide();
		$(".daily").show();
	});
	$('.fhourly').click(function(){
		$(".annual, .biannual, .quarterly, .monthly, .weekly, .daily, .minutely").hide();
		$(".hourly").show();
	});
	$('.fminutely').click(function(){
		$(".annual, .biannual, .quarterly, .monthly, .weekly, .daily, .hourly").hide();
		$(".minutely").show();
	});*/
	
});