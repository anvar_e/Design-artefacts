$(function(){
	
	$('.table-selectable').on('click', 'tr', function(event) {
		$(this).addClass('selected').siblings().removeClass('selected');
	});
	
	$("#txtEditor").Editor({
		'print':false,
		'font_size':false,
		'fonts':false,
		'select_all':false,
		'rm_format':false

	});
	
	$('#collapseDimension').on('hide.bs.collapse', function (e) {
		e.preventDefault();
	});

	var options = {
		currElClass: 'green',
		placeholderClass: 'bgC2',
		listsClass:'pah',
		isAllowed: function(cEl, hint, target)
			{
				hint.css('background-color', '#c9db8b');
				return true;
			},
		opener: {
			 active: true,
			 close: './images/minus.png',
			 open: './images/plus.png',
			 openerCss: {
				 'display': 'inline-block',
				 'width': '18px',
				 'height': '18px',
				 'float': 'left',
				 'margin-left': '-85px',
				 'margin-right': '5px',
				 'background-position': 'center center',
				 'background-repeat': 'no-repeat',
				 'cursor': 'pointer'
			 },
			 openerClass: ''
		}
	};
	$('#sTree1').sortableLists(options);
});

$(function () {
	$('[data-toggle="tooltip"]').tooltip({html: true})
});

$(function() {
	$( "#slider-range" ).slider({
		range: true,
		min: 2001,
		max: 2015,
		values: [ 2003, 2013 ],
		slide: function( event, ui ) {
			$( "#amount1" ).text( ui.values[ 0 ]);
			$( "#amount2" ).text( ui.values[ 1 ]);
		}
	});
	$( "#amount1" ).text($( "#slider-range" ).slider( "values", 0 ));
	$( "#amount2" ).text($( "#slider-range" ).slider( "values", 1 ));
});

$(document).on('change', '.btn-file :file', function() {
	var input = $(this),
		numFiles = input.get(0).files ? input.get(0).files.length : 1,
		label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function() {

	$('#btn-next-1').click(function(e){
		e.preventDefault();
		$('a[href="#tab2"]').tab('show');
	});
	
	$('#security').click(function(e){
		e.preventDefault();
		$('a[href="#tab7"]').tab('show');
		$('.nav-settings li').removeClass('active');
	});
	
	$('.nav-settings li a').click(function(e){
		$('.nav-security li').removeClass('active');
	});
	$('.nav-security li a').click(function(e){
		$('.nav-settings li').removeClass('active');
	});
	
	$("[name=toggler]").click(function(){
			$('.toHide').collapse('hide');
			//$("#blk-"+$(this).val()).show();
			$("#blk-"+$(this).val()).collapse('show');
	});
	
	$.fn.bootstrapSwitch.defaults.offText = 'NO';
	$.fn.bootstrapSwitch.defaults.onText = 'YES';
	$("[name='my-checkbox'], [name='core-enable']").bootstrapSwitch();
			
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
	
	$('#sTree2 li div').append('<div class="btn-group"><button type="button" class="btn btn-default stat-btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-edit"></i> <span class="caret"></span></button><ul class="dropdown-menu"><li class="disabled"><a href="#" data-toggle="modal" data-target="#edit-directorate"><span class="fa fa-plus" aria-hidden="true"></span> New Group</a></li><li><a href="#"><span class="fa fa-plus" aria-hidden="true"></span> Add Query</a></li><li><a href="#"><span class="fa fa-plus" aria-hidden="true"></span> Add Link</a></li><li role="separator" class="divider"></li><li role="separator" class="divider"></li><li><a href="#" data-toggle="modal" data-target="#copy"><span class="glyphicon glyphicon-copy" aria-hidden="true"></span> Copy</a></li><li class="disabled"><a href="#"><span class="glyphicon glyphicon-paste" aria-hidden="true"></span> Paste</a></li><li><a href="#" data-toggle="modal" data-target="#rename"><span class="fa fa-file-text" aria-hidden="true"></span> Rename</a></li><li><a href="#" data-toggle="modal" data-target="#confirm"><span class="fa fa-trash" aria-hidden="true"></span> Delete</a></li><li role="separator" class="divider"></li><li role="separator" class="divider"></li><li><a href="#" data-toggle="modal" data-target="#properties">Properties</a></li></ul></div>');
	
	$('.table-workflow tbody tr td:nth-child(3)').prepend('<div class="btn-group"><button type="button" class="btn btn-default stat-btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-edit"></i> <span class="caret"></span></button><ul class="dropdown-menu"><li><a href="#" data-toggle="modal" data-target="#confirm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Change to <span class="pending">pending</span></a></li><li><a href="#" data-toggle="modal" data-target="#confirm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Change to <span class="text-success">approved</span></a></li><li><a href="#" data-toggle="modal" data-target="#confirm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Change to <span class="text-danger">rejected</span></a></li><li role="separator" class="divider"></li><li><a href="#" data-toggle="modal" data-target="#notes"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> Notes</a></li></ul></div>&nbsp;&nbsp;');
	
	
	//$('#sTree2 li div').not('.btn-group').append('<span class="sortableListsOpener" style="float: left; display: inline-block; width: 18px; height: 18px; margin-left: -85px; margin-right: 5px; cursor: pointer; background-image: url(http://localhost/_admin/images/minus.png); background-position: 50% 50%; background-repeat: no-repeat;"></span>');
	
	$('#sTree2 > li').each(function(){
		if ($(this).find('ul').length > 0){
			$('#sTree2 > li > div').prepend('<span class="sortableListsOpener" style="float: left; display: inline-block; width: 18px; height: 18px; margin-left: -85px; margin-right: 5px; cursor: pointer; background-image: url(http://localhost/_admin/images/minus.png); background-position: 50% 50%; background-repeat: no-repeat;"></span>');
		}
	});
	
	$('#sTree1 li div').append('<div class="btn-group"><button type="button" class="btn btn-default stat-btn inactive"><i class="fa fa-edit"></i> <span class="caret"></span></button></div>');
	
	$(function(){
		$('#copy').on('show.bs.modal', function(){
			var myModal = $(this);
			clearTimeout(myModal.data('hideInterval'));
			myModal.data('hideInterval', setTimeout(function(){
				myModal.modal('hide');
			}, 1800));
		});
	});
	
	
	// sticky top menu
	var nav_container = $('.nav-container');
	var nav = $('nav.top');
	
	var top_spacing = 0;
	var waypoint_offset = 283;

	nav_container.waypoint({
		handler: function(event, direction) {
			if (direction == 'down') {
				nav_container.css({ 'height':'60px' });
				nav.stop().addClass("sticky").css("top",-nav.outerHeight()).animate({"top":""}, "slow");
			} else {
				nav_container.css({ 'height':'60px' });
				nav.stop().removeClass("sticky").css("top",nav.outerHeight()+"550").animate({"top":"0"}, "slow");
			}
		},
		offset: function() {
			return -nav.outerHeight()-waypoint_offset;
		}
	});
	
	$("#top").hide();
	$(window).scroll(function () {
		if ($(this).scrollTop() > 140) { // shows the top button in the right bottom corner when needed
			$("#top").fadeIn("slow");
		} else {
			$("#top").fadeOut("slow");
		}
	});
	
	
	$("a[href=#top], #top").click(function(){
		$("html, body").animate({scrollTop:0}, "slow"); // scrolls to the top of the page
		return false;
	});
	
	$('select.searchable').select2();
	
	$(function() {
		$( "#from" ).datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 3,
			onClose: function( selectedDate ) {
				$( "#to" ).datepicker( "option", "minDate", selectedDate );
			}
		});
		$( "#to" ).datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 3,
			onClose: function( selectedDate ) {
				$( "#from" ).datepicker( "option", "maxDate", selectedDate );
			}
		});
	});
	
	$(function(){
		$(window).bind("resize",function(){
			console.log($(this).width())
			if($(this).width() <992){
				$('#main, #menu, #header').removeClass('container').addClass('container-fluid')
			}
			else{
				$('#main, #menu, #header').removeClass('container-fluid').addClass('container')
			}
		});
	});
	
});

var options = {
  valueNames: [ 'code', 'name', 'theme', 'directorate', 'owner' ]
};

var userList = new List('datasets', options);
var userList2 = new List('workflow', options);