jQuery(function($) {
	function tog(v){return v?'addClass':'removeClass';} 
		$(document).on('input', '.clearable', function(){
			$(this)[tog(this.value)]('x');
		}).on('mousemove', '.x', function( e ){
			$(this)[tog(this.offsetWidth-18 < e.clientX-this.getBoundingClientRect().left)]('onX');   
		}).on('touchstart click', '.onX', function( ev ){
			ev.preventDefault();
		$(this).removeClass('x onX').val('').change();
	});
});
$(document).ready(function() {
	
	$('.group').hide();
	$('.option1').show();
	$('#selectMe').change(function () {
		$('.group').hide();
		$('.'+$(this).val()).show();
  	});
	
	$('.container-advsearch').on('shown.bs.collapse', function() {
		$(".btn-specify i").addClass('fa-chevron-up').removeClass('fa-chevron-down');
	});

	$('.container-advsearch').on('hidden.bs.collapse', function() {
		$(".btn-specify i").addClass('fa-chevron-down').removeClass('fa-chevron-up');
	});
	
	$('#ctl00_CPH1_T_Country_Key').multiselect({
		buttonWidth: '100%',
		buttonClass: 'btn btn-default btn-mselect',
		nonSelectedText: 'Select one or more countries',
		includeSelectAllOption: true,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: false,
		filterPlaceholder: 'filter countries...',
		templates: {
			filter: '<li class="multiselect-item filter"><input class="form-control clearable input-sm" type="text"></li>'
		}
	});
	
	$("#top").hide();
	$(window).scroll(function () {
		if ($(this).scrollTop() > 140) { // shows the top button in the right bottom corner when needed
			$("#top").fadeIn("slow");
		} else {
			$("#top").fadeOut("slow");
		}
	});
	
	
	$("a[href=#top], #top").click(function(){
		$("html, body").animate({scrollTop:0}, "slow"); // scrolls to the top of the page
		return false;
	});
	
	/*$("#disclaimer").click(function() {
		$('html, body').animate({
			scrollTop: $("#disclaimer").offset().top
		}, "slow");
		return false;
	});*/
	

	var nav = $('.nav-container');
	var content = $('.content-wrap');
	$(window).scroll(function () {
		if ($(this).scrollTop() > 83) {
			nav.addClass("fixed-nav");
			content.addClass("topmargin");
		} else {
			nav.removeClass("fixed-nav");
			content.removeClass("topmargin");
		}
	});


});