# Design-artefacts

- [Global Recall](https://github.com/cis-itn-oecd/Design-artefacts/tree/global_recall)
- [Extended Data Explorer](https://github.com/cis-itn-oecd/Design-artefacts/tree/extended_data_explorer)
- [DLM](https://github.com/cis-itn-oecd/Design-artefacts/tree/dlm)
- [STRI](https://github.com/cis-itn-oecd/Design-artefacts/tree/stri)
- [e.AOP Portal](https://github.com/cis-itn-oecd/Design-artefacts/tree/e_aop_portal)

# Demo site

- [LIVE](https://cis-itn-oecd.github.io/Design-artefacts)